<!--
author:   Axel Klinger

version:  0.1

language: de

comment:  Einstieg in GitLab für Open Science Projekte
-->
# GitLab für Open Science

> Egal ob du studierst, lehrst oder forscht, **ob du die Welt ein Stück besser machen willst mit deinem Wissen und deinen Erkenntnissen** oder **Leute erreichen möchtest, die sich für dein Thema interessieren - für guten Austausch, Feedback und zur Verbesserung deiner eigenen Arbeit**,  **teile dein Wissen und deine Erkenntnisse mit der Welt!**

In diesem Tutorial erfährst du, wie du mit einfachen Mitteln deine Arbeit offen, transparent und nachnutzbar machst - mit ersten Ergebnissen in weniger als 3 Minuten startest, nach 15 Minuten bereits zahlreiche Komponenten nutzen kannst und nach weniger als einer Stunde eine produktive Arbeitsumgebung und die erste Eintragung in einschlägigen Suchportalen hast.

* Zielgruppe: **Studierende**, **Lehrende**, **Forschende**
* Grundkenntnisse: keine, aber Wille, gute Veröffentlichung (Paper, Kurs, Klausurvorbereitung, Abschlussarbeit etc.) gut zu veröffentlichen

**Vorteile**

* Unterstützung bei ...

  - bessere Qualität in Form und Inhalt
  - bessere Lesbarkeit für Mensch und Maschine
  - attraktivere Ausgabeformate für Zielgruppe
  - mehr Offenheit für alle
  - bessere Auffindbarkeit im Netz und in Zielgruppen

**Nachteile**

* neue Technik kennenlernen, aber mit 1h Aufwand überschaubar ;-)


=> **eigentliches Ziel: SDG 4**

## Getting Started

Das Ziel von Getting started ist, in weniger als 3 Minuten eigendes Material auf GitLab zu haben, nach 15 Minuten bereits fortgeschrittene Inhalte in attraktiven Formaten mit zahlreichen Möglichkeiten zur Interaktion zu erzeugen und nach ca. 60 Minuten eine produktive Arbeitsumgebung zu haben, mit der ich meine gesamte Forschung und Lehre offen und nachhaltig gestalten kann. -> Sprung über Theorie und Hintergrund zu Handlung!

* als Studierende
* als Lehrende
* als Forschende

### **Studierende** - Projekt- oder Abschlussarbeit

### **Lehrende** - Kurs oder Lektion

### **Forschende** - Antrag, Bericht oder Dokumentation

---

## Was ist GitLab

**GitLab** ist eine Webanwendung zur [Versionsverwaltung](https://de.wikipedia.org/wiki/Versionsverwaltung) für Textdateien auf der Basis von Git. Neben der Versionsverwaltung des Verzeichnises (Repository) bietet GitLab weitere Funktionen zur Aufgabenplanung in Form eines Issue-Tracking-Systems mit [Kanban-Board](https://de.wikipedia.org/wiki/Kanban-Tafel), Automatisierungsmechanismen für die Umwandlung der Quelltexte und die Bereitstellung der Ergebnisse (Continuous Integration und Continuous Delivery (CI/CD)) und ein Wiki für die Dokumentation des Projekts selbst. Darüber hinaus bietet GitLab noch zahlreiche weitere Features, insbesondere für die Software Entwicklung, auf die im Rahmen dieses Tutorials nicht näher eingegangen wird.

Zusammengefasst bietet GitLab

* Versionsverwaltung für Textdateien
* Projektmanagement
* Wiki
* Automatisierung

### Die Oberfläche von GitLab

... die wesentlichen Funktionen einmal kurz erklärt ...

> **Slides mit den Features der Oberfläche**

-> generieren und einbinden!

### Erste Aktionen s.o.

**Registrierung**

**Das erste eigene Projekt**

<Video>

**Dateien anlegen und bearbeiten**

... am Beispiel Markdown ...

## Projektmanagement

Am (virtuellen) Beispiel OSP

### Liste von Aufgaben

### Übersicht mit Board

### Kategorien mit Labels

### Planung mit Meilensteinen


## Dokumentation

### Markdown

### Im Wiki

... Dokumentation der Bearbeitung ...

### Im Repository

... Dokumentation der Ergebnisse ...

## Kooperation

Bei allen Projekten ist es wichtig, dass man auch Feedback bekommt, um besser zu werden. Aber nicht nur Feedback, somdern auch aktive Beteiligung ist möglich - inverschiedenen Stufen.

* Anmerkung melden (Verbesserung, Fehler, Feedback ...) via Ticket
* Verbesserungsvorschlag über Merge Request
* Beteiligung durch Projektmitgliedschaft und Bearbeitungsrechte

## Administration

### Projekteinstellungen

### Benutzerverwaltung

In jedem Projekt kannst du

## Lokale Arbeitsumgebung

Mit der Gitlab-Weboberfläche lässt sich alles machen, aber ein lokaler Editor bringt darüber hinaus noch einige Vorteile, wenn du mehr mit dieser Technilogie arbeiten möchtest und dieses so effizient wie möglich. Erweiterte Markdown-Inhalte lassen sich mit einem lokalen Editor deutlich einfacher bearbeiten. Der Mehraufwand der einmaligen Einrichtung der Arbeitsumgebung und der wenigen Schritte mehr beim Speichern haben sich bei regelmäßiger Nutzung bereits nach wenigen Stunden amortisiert.

### Installation Atom

* Standardinstallation von [Atom](https://atom.io)
* Plugins für Open Science mit Markdown

// in diesem Tutorial beschränken wir uns auf Markdown. Andere Auszeichnungsformate, wie z.B. ASCIIdoc, LaTeX oder Textile lassen sich mit geringen Anpassungen des Templates auf die gleiche Weise erstellen.

### Installation Visual Studio Code

## Grundlagen Git

... s.a. Git-Book ...

... s.a. GitLab-Folien NFDI4Ing
